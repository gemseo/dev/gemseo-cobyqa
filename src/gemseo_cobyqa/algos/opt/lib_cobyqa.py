# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Copyright 2022 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Francois Gallard
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
# Contributors:
#    INITIAL AUTHORS - initial API and implementation and/or initial
#                           documentation
#        :author: Damien Guenot
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
#         Francois Gallard : refactoring for v1, May 2016
"""COBYQA optimization library wrapper."""

from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING
from typing import Any
from typing import ClassVar

from cobyqa import minimize
from gemseo.algos.opt.optimization_library import OptimizationAlgorithmDescription
from gemseo.algos.opt.optimization_library import OptimizationLibrary
from numpy import inf
from numpy import vstack

if TYPE_CHECKING:
    from gemseo.algos.opt_result import OptimizationResult


@dataclass
class COBYQAlgorithmDescription(OptimizationAlgorithmDescription):
    """The description of an optimization algorithm from the COBYQA library."""

    library_name: str = "COBYQA"


class COBYQAOpt(OptimizationLibrary):
    """COBYQA optimization library interface.

    See OptimizationLibrary.
    """

    OPTIONS_MAP: ClassVar[dict[str, Any]] = {}

    def __init__(self) -> None:  # noqa:D107
        super().__init__()
        doc = "cobyqa.com"
        self.descriptions = {
            "COBYQA": COBYQAlgorithmDescription(
                algorithm_name="COBYQA",
                description="COBYQA is a derivative-free trust-region SQP method "
                "based on quadratic models obtained by underdetermined interpolation.",
                handle_equality_constraints=True,
                handle_inequality_constraints=True,
                internal_algorithm_name="COBYQA",
                require_gradient=False,
                positive_constraints=True,
                website=f"{doc}/stable/ref/generated/cobyqa.minimize.html",
            )
        }

    def _get_options(
        self,
        max_iter: int = 999,
        ftol_rel: float = 1e-9,
        ftol_abs: float = 1e-9,
        xtol_rel: float = 1e-9,
        xtol_abs: float = 1e-9,
        disp: bool = False,
        normalize_design_space: bool = True,
        eq_tolerance: float = 1e-2,
        ineq_tolerance: float = 1e-4,
        kkt_tol_abs: float | None = None,
        kkt_tol_rel: float | None = None,
        radius_init: float = 1.0,
        radius_final: float = 1e-6,
        nb_points: int | None = None,
        stop_crit_n_x: int | None = None,
        target: float = -inf,
        debug: bool = False,
        **kwargs: Any,
    ) -> dict[str, Any]:
        r"""Set the options default values.

        To get the best and up-to-date information about algorithms options,
        refer to the
        `COBYQA documentation <cobyqa.com/stable/ref/generated/cobyqa.minimize.html>`_.

        Args:
            max_iter: The maximum number of iterations, i.e. unique calls to f(x).
            ftol_rel: A stop criteria, the relative tolerance on the
               objective function.
               If abs(f(xk)-f(xk+1))/abs(f(xk))<= ftol_rel: stop.
            ftol_abs: A stop criteria, the absolute tolerance on the objective
               function. If abs(f(xk)-f(xk+1))<= ftol_rel: stop.
            xtol_rel: A stop criteria, the relative tolerance on the
               design variables. If norm(xk-xk+1)/norm(xk)<= xtol_rel: stop.
            xtol_abs: A stop criteria, absolute tolerance on the
               design variables.
               If norm(xk-xk+1)<= xtol_abs: stop.
            disp: Whether to print pieces of information on the execution of the solver.
            normalize_design_space: If True, scales variables to [0, 1].
            eq_tolerance: The equality tolerance.
            ineq_tolerance: The inequality tolerance.
            kkt_tol_abs: The absolute tolerance on the KKT residual norm.
                If ``None`` this criterion is not activated.
            kkt_tol_rel: The relative tolerance on the KKT residual norm.
                If ``None`` this criterion is not activated.
            radius_init: Initial trust-region radius.
            radius_final: Final trust-region radius.
            nb_points: Number of interpolation points for the objective and
                constraint models, if None, 2*n+1 is used.
            stop_crit_n_x: The minimum number of design vectors to take into account in
                the stopping criteria.
            target: Target value on the objective function (the default is -numpy.inf).
                If the solver encounters a feasible point at which the objective
                function evaluations is below the target value, then the computations
                are stopped.
            debug: Whether to make debugging tests during the execution,
                which is not recommended in production.
            **kwargs: The other algorithm options.
        """
        if nb_points is None:
            nb_points = self.problem.design_space.dimension * 2 + 1

        if stop_crit_n_x is None:
            stop_crit_n_x = nb_points + 1

        return self._process_options(
            max_iter=max_iter,
            ftol_rel=ftol_rel,
            ftol_abs=ftol_abs,
            xtol_rel=xtol_rel,
            xtol_abs=xtol_abs,
            disp=disp,
            normalize_design_space=normalize_design_space,
            ineq_tolerance=ineq_tolerance,
            eq_tolerance=eq_tolerance,
            kkt_tol_abs=kkt_tol_abs,
            kkt_tol_rel=kkt_tol_rel,
            nb_points=nb_points,
            stop_crit_n_x=stop_crit_n_x,
            radius_init=radius_init,
            radius_final=radius_final,
            target=target,
            debug=debug,
            **kwargs,
        )

    def _run(self, **options: Any) -> OptimizationResult:
        """Run the algorithm.

        Args:
            **options: The options for the algorithm.

        Returns:
            The optimization result.
        """
        # Get the normalized bounds:
        x_0, l_b, u_b = self.get_x0_and_bounds_vects(
            options.pop(self.NORMALIZE_DESIGN_SPACE_OPTION)
        )

        # Make sure |g| stops COBYQA
        options["maxiter"] = 500 * self.problem.design_space.dimension
        options["maxfev"] = options["maxiter"]

        # Remove |g|-specific options
        options.pop(self.F_TOL_ABS)
        options.pop(self.X_TOL_ABS)
        options.pop(self.F_TOL_REL)
        options.pop(self.X_TOL_REL)
        options.pop(self.MAX_ITER)
        options.pop(self._KKT_TOL_REL)
        options.pop(self._KKT_TOL_ABS)
        options.pop(self.STOP_CRIT_NX)

        opt_result = minimize(
            self.problem.objective.__call__,
            x_0,
            (),
            vstack([l_b, u_b]).T,
            [
                {"type": "ineq", "fun": constraint}
                for constraint in self.get_right_sign_constraints()
            ]
            + [
                {"type": "eq", "fun": constraint}
                for constraint in self.problem.get_eq_constraints()
            ],
            None,
            options,
        )
        return self.get_optimum_from_database(opt_result.message, opt_result.status)
