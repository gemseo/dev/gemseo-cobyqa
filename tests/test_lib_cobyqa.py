# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Copyright 2022 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Francois Gallard
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
"""Tests for the wrapping of COBYQA."""

from math import cos
from math import sin

import pytest
from cobyqa import minimize
from gemseo import execute_algo
from gemseo.algos.design_space import DesignSpace
from gemseo.algos.opt_problem import OptimizationProblem
from gemseo.core.mdofunctions.mdo_function import MDOFunction
from gemseo.problems.analytical.power_2 import Power2
from numpy.linalg import norm
from scipy.optimize import rosen

from gemseo_cobyqa.algos.opt.lib_cobyqa import COBYQAOpt


def test_basic_cobyqa_rosen():
    """Basic test of COBYQA without GEMSEO."""
    x0 = [1.3, 0.7, 0.8, 1.9, 1.2]
    res = minimize(rosen, x0)
    assert norm(res.x - 1.0) < 1e-4


def test_stop_crit_n():
    """Test that the ``stop_crit_n`` option is taken into account by default.

    This ensures that ``COBYQA`` performs at least nb_points+1 points before checking
    the convergence criteria.
    """
    design_space = DesignSpace()
    nb_var = 10
    design_space.add_variable("x", l_b=0, u_b=1, value=0.5, size=nb_var)
    problem = OptimizationProblem(design_space)
    problem.objective = MDOFunction(lambda x: sum(x), "my_func")
    execute_algo(opt_problem=problem, algo_name="COBYQA")

    assert problem.current_iter > 2 * nb_var + 2


def test_scalar_ineq():
    """Test GEMSEO interface on a problem with scalar values constraints."""
    design_space = DesignSpace()
    design_space.add_variable("x", l_b=0, u_b=1, value=0.5)
    problem = OptimizationProblem(design_space)
    problem.objective = MDOFunction(sin, "sin")
    problem.add_constraint(MDOFunction(cos, "cos"), cstr_type="eq", value=0.5)
    problem.add_constraint(MDOFunction(cos, "cos"), cstr_type="ineq", value=2)
    execute_algo(opt_problem=problem, algo_name="COBYQA")


@pytest.fixture()
def problem() -> Power2:
    """The Power2 optimization problem."""
    return Power2()


def test_radius_init(problem):
    """Test that the ``radius_init`` option is cascaded properly."""
    execute_algo(opt_problem=problem, algo_name="COBYQA", max_iter=2, radius_init=0.1)
    assert problem.database.get_x_vect(2)[0] == 0.8


def test_radius_final(problem):
    """Test the ``radius_final`` option is cascaded properly."""
    result_1 = execute_algo(opt_problem=problem, algo_name="COBYQA", radius_final=1e-1)
    problem.reset()
    result_2 = execute_algo(opt_problem=problem, algo_name="COBYQA", radius_final=1e-2)
    assert result_2.n_obj_call > result_1.n_obj_call


def test_power2(problem):
    """Test GEMSEO interface on the Power2 problem."""
    result = execute_algo(opt_problem=problem, algo_name="COBYQA")
    assert abs(problem.get_solution()[1] - result.f_opt) < 1.5e-3


@pytest.fixture()
def library(problem) -> COBYQAOpt:
    """The COBYQA wrapper."""
    library = COBYQAOpt()
    library.algo_name = "COBYQA"
    library.init_options_grammar("COBYQA")
    library.problem = problem
    return library


@pytest.mark.parametrize(("user_value", "gemseo_value"), [(None, 7), (9, 9)])
def test_nb_points(library, user_value, gemseo_value):
    """Check the number of interpolation points."""
    assert library._get_options(nb_points=user_value)["nb_points"] == gemseo_value


@pytest.mark.parametrize(
    ("options", "stop_crit_n_x"),
    [({"stop_crit_n_x": 3}, 3), ({"nb_points": 9}, 10), ({}, 8)],
)
def test_stop_crit_nx(library, options, stop_crit_n_x):
    """Check the number of iterations taken into account for stopping."""
    assert library._get_options(**options)["stop_crit_n_x"] == stop_crit_n_x
